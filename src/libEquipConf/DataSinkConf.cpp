#include "DataSinkConf.h"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>

#include "Logger.h"

#include "DataSinkRegistry.h"
#include "CombinedSink.h"

#include "Logger.h"

using json = nlohmann::json;

using json = nlohmann::json;

////////////////////
// Configuration
////////////////////

DataSinkConf::DataSinkConf()
{ }

DataSinkConf::~DataSinkConf()
{ }

DataSinkConf::DataSinkConf(const std::string& hardwareConfigFile)
{
  setHardwareConfig(hardwareConfigFile);
}

DataSinkConf::DataSinkConf(const json& hardwareConfig)
{
  setHardwareConfig(hardwareConfig);
}

void DataSinkConf::setHardwareConfig(const std::string& hardwareConfigFile)
{
  //load JSON object from file
  std::ifstream i(hardwareConfigFile);
  if(!i.good()) {
    throw std::runtime_error("Provided datasink configuration file \"" + hardwareConfigFile + "\" could not be found or opened");
  }
  i >> m_hardwareConfig;
}

void DataSinkConf::setHardwareConfig(const json& hardwareConfig)
{
  //store JSON config file
  m_hardwareConfig = hardwareConfig;
}

json DataSinkConf::getDataSinkConf(const std::string& label)
{
  for (const auto& hw : m_hardwareConfig["datasinks"].items())
    {
      //check label 
      if (hw.key() == label) return hw.value();
    }
  return json();
}

json DataSinkConf::getDataStreamConf(const std::string& label)
{
  for (const auto& hw : m_hardwareConfig["datastreams"].items())
    {
      //check label 
      if (hw.key() == label) return hw.value();
    }
  return json();
}

////////////////////
// General private
////////////////////

std::shared_ptr<IDataSink> DataSinkConf::getDataSink(const std::string& name)
{
  //first check if an object with the same name/type is already available (was already instantiated)
  if (m_dataSinks.find(name) != m_dataSinks.end())
    return m_dataSinks[name];

  //Otherwise, create the object
  //check first if hardware configuration is available

  json reqSinkConf = getDataSinkConf(name);
  if (reqSinkConf.empty())
    {
      logger(logWARNING) << "Requested data sink not found in input configuration file: " << name;
      return nullptr;
    }


  // setup sink
  std::shared_ptr<IDataSink> ds = EquipRegistry::createDataSink(reqSinkConf["sinktype"], name);

  // configure and initialize
  ds->setConfiguration(reqSinkConf);
  ds->init();

  return ds;
}

std::shared_ptr<CombinedSink> DataSinkConf::getDataStream(const std::string& name)
{
  //first check if an object with the same name/type is already available (was already instantiated)
  if (m_dataStreams.find(name) != m_dataStreams.end())
    return m_dataStreams[name];

  //Otherwise, create the object
  //check first if hardware configuration is available

  json reqStreamConf = getDataStreamConf(name);
  if (reqStreamConf.empty())
    {
      logger(logWARNING) << "Requested data stream not found in input configuration file: " << name;
      return nullptr;
    }


  // setup sink
  std::shared_ptr<CombinedSink> ds =
    std::dynamic_pointer_cast<CombinedSink>(EquipRegistry::createDataSink(reqStreamConf.value("sinktype", "CombinedSink"), name));

  // Check if combined sink
  if(!ds)
    {
      logger(logWARNING) << "Requested data stream does not derive from CombinedSink: " << name;
      return nullptr;
    }
    

  // Add sub sinks
  auto sinks=reqStreamConf.find("sinks");
  if(sinks==reqStreamConf.end())
    {
      logger(logWARNING) << "No sub-sinks defined in " << name << " combined sink.";
    }
  else
    {
      for(const auto& sink : sinks.value().items())
	{
	  std::shared_ptr<IDataSink> sds = getDataSink(sink.value());
	  if(sds==nullptr) continue;
	  ds->addSink(sds);
	}
    }

  // configure and initialize
  ds->setConfiguration(reqStreamConf);
  ds->init();

  return ds;
}
