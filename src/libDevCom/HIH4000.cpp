#include "HIH4000.h"
#include "DeviceComRegistry.h"
REGISTER_DEVCOM(HIH4000, ClimateSensor)

#include "NotSupportedException.h"

#include <cmath>


HIH4000::HIH4000(uint8_t chan, std::shared_ptr<ADCDevice> dev, std::shared_ptr<ClimateSensor> tempSens, float Vsup)
  : m_adcdev(dev), m_tempSens(tempSens), m_chan(chan), m_Vsup(Vsup), m_calib(false)
{ }

HIH4000::HIH4000(uint8_t chan, std::shared_ptr<ADCDevice> dev, std::shared_ptr<ClimateSensor> tempSens, float offset, float slope)
  : m_adcdev(dev), m_tempSens(tempSens), m_chan(chan), m_offset(offset), m_slope(slope), m_calib(true)
{ }

HIH4000::~HIH4000()
{ }

void HIH4000::init()
{ }

void HIH4000::reset()
{ }

void HIH4000::read()
{
  m_tempSens->read();
  m_temperature=m_tempSens->temperature();
  m_humidity = 0.;
  const uint navg = 5;
  // output voltage
  for(uint i=0;i<navg;i++)
    m_humidity += (float)m_adcdev->read(m_chan);
  m_humidity/=(float)navg;
  // sensor humidity
  if (m_calib) m_humidity = (m_humidity - m_offset)/m_slope;
  else m_humidity = (m_humidity/m_Vsup-0.16)/0.0062;  
   // temperature correction
  m_humidity=m_humidity/(1.0546-0.00216*m_temperature);
}

float HIH4000::temperature() const
{
  return m_temperature;
}

float HIH4000::humidity() const
{
  return m_humidity;
}

float HIH4000::pressure() const
{
  throw NotSupportedException("Pressure not supported for HIH4000");
  return 0;
}
