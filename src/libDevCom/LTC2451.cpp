#include "LTC2451.h"
#include "DeviceComRegistry.h"
REGISTER_DEVCOM(LTC2451, ADCDevice)

#include "LinearCalibration.h"

#include "NotSupportedException.h"
#include "OutOfRangeException.h"
#include "ComIOException.h"

#include <thread>
#include <chrono>

LTC2451::LTC2451(double reference, std::shared_ptr<I2CCom> com)
  : ADCDevice(std::make_shared<LinearCalibration>(reference, 0xFFFF)),
    m_com(com)
{
  setSpeed(Speed30Hz);
}

LTC2451::~LTC2451()
{ }

int32_t LTC2451::readCount()
{
  try
    { // dummy read to start conversion cycle
      m_com->read_reg16();
    }
  catch(const ComIOException& e)
    { /* Read might fail if conversion already in progress */ }
  wait_for_finish();
  return m_com->read_reg16();
}

int32_t LTC2451::readCount(uint8_t ch)
{
  if(ch!=0)
    throw OutOfRangeException(ch,0,0);
  return readCount();
}

void LTC2451::readCount(const std::vector<uint8_t>& chs, std::vector<int32_t>& counts)
{
  counts.clear();
  for(uint8_t ch : chs)
    counts.push_back(readCount(ch));
}

void LTC2451::setSpeed(Speed speed)
{
  try
    {
      m_com->write_reg8(speed);
    }
  catch(const ComIOException& e)
    { // Allow write to fail once, in case conversion in progress
      wait_for_finish();
      m_com->write_reg8(speed);
    } 
}

void LTC2451::wait_for_finish()
{
  std::this_thread::sleep_for(std::chrono::milliseconds(1000/30+15)); // assume 30Hz rate
}
