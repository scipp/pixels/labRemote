#include "DataSinkRegistry.h"

#include <iostream>

namespace EquipRegistry
{

  typedef ClassRegistry<IDataSink, std::string> RegistryDataSink;

  static RegistryDataSink &registry()
  {
    static RegistryDataSink instance;
    return instance;
  }

  bool registerDataSink(const std::string& type, std::function<std::shared_ptr<IDataSink>(const std::string&)> f)
  {
    return registry().registerClass(type, f);
  }
  
  std::shared_ptr<IDataSink> createDataSink(const std::string& type, const std::string& name)
  {
    auto result = registry().makeClass(type, name);
    if(result == nullptr)
      std::cout << "No data formats (IDataSink) of type " << type << " found\n";

    return result;
  }
  
  std::vector<std::string> listDataSink()
  {
    return registry().listClasses();
  }

}

