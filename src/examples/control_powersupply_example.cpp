#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <iostream>
#include <memory>
#include <string>

#include "Logger.h"
#include "EquipConf.h"
#include "PowerSupplyChannel.h"
#include "AgilentPs.h"

//------ SETTINGS
std::string configFile;
std::string channelName;
//---------------


void usage(char *argv[])
{
  std::cerr << "Usage: " << argv[0] << " configfile.json channelname" << std::endl;
  std::cerr << "" << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -d, --debug       Enable more verbose printout, use multiple for increased debug level"  << std::endl;
  std::cerr << "" << std::endl;
  std::cerr << "" << std::endl;
}

int main(int argc, char*argv[])
{

  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  //Parse command-line
  int c;
  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
      {"debug",    no_argument      , 0,  'd' },
      {0,          0,                 0,  0 }
    };
    
    c = getopt_long(argc, argv, "d", long_options, &option_index);
    if (c == -1)
      break;

    switch (c)
      {
      case 'd':
	logIt::incrDebug();
	break;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	std::cerr << std::endl;
	usage(argv);
	return 1;
      }
  }

  if(optind>argc-2)
    {
      logger(logERROR) << "Missing positional arguments.";
      usage(argv);
      return 1;
    }

  configFile =argv[optind++];
  channelName=argv[optind++];

  logger(logDEBUG) << "Settings:";
  logger(logDEBUG) << " Config file: " << configFile;
  logger(logDEBUG) << " PS channel: " << channelName;
  
  //
  // Create hardware database
  EquipConf hw;
  hw.setHardwareConfig(configFile);

  logger(logDEBUG) << "Configuring power-supply.";
  std::shared_ptr<PowerSupplyChannel> PS = hw.getPowerSupplyChannel(channelName);
  
  logger(logDEBUG) << "Program power-supply.";
  PS->program();

  
  //Now interpret command
  logger(logDEBUG) << "Sending commend to power-supply.";

  logger(logINFO) << "Program";
  PS->program();
  logger(logINFO) << "Turn ON";
  PS->turnOn();
  logger(logINFO) << "Voltage: " << PS->measureVoltage();
  logger(logINFO) << "Current: " << PS->measureCurrent();

  //To acces a function that is only defined for a specific powersupply (Agilent in this case), need to cast back 
  std::shared_ptr<AgilentPs> Agilent = std::dynamic_pointer_cast<AgilentPs>(PS->getPowerSupply());
  if (Agilent == nullptr) 
    logger(logWARNING)<<"The power supply is not an Agilent, can't execute required action";
  else { 
    logger(logINFO) << "Switch off beeping for Agilent PS";
    Agilent->beepOff();
  }

  logger(logINFO) << "Turn OFF";
  PS->turnOff();

  logger(logINFO) << "All done";  
  
  return 0;
}
