#ifndef CHILLER_H
#define CHILLER_H

#include <string>
#include <memory>
#include <stdexcept>

#include "ICom.h"

//! \brief Abstract class for interfacing with a chiller
/**
 * All classes that inherit from this class have Python bindings.
 */
class IChiller
{
public:
  IChiller();
  virtual ~IChiller();

  //! Initialize the chiller and the interface
  virtual void init()=0;
  //! Set the communication object
  /**
   * 
   * The object is initialized if not already.
   * 
   * \param com ICom instance for communicating with the chiller
   */
  void setCom(std::shared_ptr<ICom> com);
  //! Turn the chiller on
  virtual void turnOn()=0;
  //! Turn the chiller off
  virtual void turnOff()=0;
  //! Set the target temperature of the chiller
  /**
   * \param temp The target temperature in Celsius
   */ 
  virtual void setTargetTemperature(float temp)=0;
  //! Return the temperature that the chiller is set to in Celsius
  virtual float getTargetTemperature()=0;
  //! Return the current temperature of the chiller in Celsius
  virtual float measureTemperature()=0;
  //! Get the status of the chiller
  /*
   * \return true if chiller is on and working properly, and false otherwise
   */ 
  virtual bool getStatus()=0;

protected:
  std::shared_ptr<ICom> m_com; 

  //! Convert Celcius to Fahrenheit
  float ctof(float t);
  //! Convert Fahrenheit to Celcius
  float ftoc(float t);

};

#endif
