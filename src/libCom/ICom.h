#ifndef ICOM_H
#define ICOM_H

#include <string>
#include <nlohmann/json.hpp>

#include "ILockable.h"

/** 
 * Interface for generic communication with a hardware device.
 *
 * Each instance is responsible for communicating with a single device. The
 * address of the recipient is part of the `ICom` configuration.
 *
 * Supports:
 *  - sending a string of bytes to the device
 *  - receiveing a string of bytes from a device
 *  - exclusive access to device hardware
 *
 * ## Initialization
 *
 * The initialization of the device should happen in the `init()` function. The
 * constructor should only create a the object, but not open the connection.
 *
 * ## Locking mechanism
 *
 * All communication functions (`receive`/`send`/`sendreceive`) are required to be 
 * multi-process safe and reentrant. That is, no other communication with the
 * device from a different process is allowed until the function completes.
 *
 * Applications can request exclusive lock to a communication class via the
 * `ILockable` interface.
 */
class ICom : public ILockable
{
public:
  ICom();
  ~ICom() =default;

  /** \brief Initialize communication.
   *
   * Implementations should check `is_open` to see if communication
   * has already been initialized. If already called, the function
   * should return without an error.
   *
   * On success, the implementation should set `m_good=true`.
   *
   * Throw `std::runtime_error` on error.
   */
  virtual void init() =0;

  /** \brief Check if the object is correctly initialized
   *
   * \return true if initialization succeeded, false otherwise
   */
  bool is_open();

  /** \brief Set communication settings from JSON object
   *
   * \param config JSON configuration
   */
  virtual void setConfiguration(const nlohmann::json& config) =0;

  /** Send data to device
   *
   * Throw `std::runtime_error` on error.
   *
   * \param buf Data to be sent
   * \param length Number of characters in `buf` that should be sent
   */  
  virtual void send(char *buf, size_t length) =0;

  /** Send data to device
   *
   * Throw `std::runtime_error` on error.
   *
   * \param buf Data to be sent
   */
  virtual void send(const std::string& buf) =0;

  /** Read data from device
   *
   * Throw `std::runtime_error` on error.
   *
   * \return Received data
   */  
  virtual std::string receive() =0;

  /** Receive data from device
   *
   * Throw `std::runtime_error` on error.
   *
   * \param buf Buffer where to store results 
   * \param length Number of characters to receive.
   *
   * \return Number of characters received
   */
  virtual uint32_t receive(char *buf, size_t length) =0;

  /** Send data to device and receive reply
   *
   * Throw `std::runtime_error` on error.
   *
   * \param cmd Data to be sent
   *
   * \return Returned data
   */
  virtual std::string sendreceive(const std::string& cmd) =0;

  /** Send data to device and receive reply
   *
   * Throw `std::runtime_error` on error.
   *
   * \param wbuf Data to be sent
   * \param wlength Number of characters in `wbuf` that should be sent
   * \param rbuf Buffer where to store results 
   * \param rlength Number of characters to receive.
   *
   * \return Number of characters received
   */
  virtual void sendreceive(char *wbuf, size_t wlength, char *rbuf, size_t rlength) =0;  

protected:
  //! Good if communication is opened and configured correctly
  bool m_good =false;
};

#endif
