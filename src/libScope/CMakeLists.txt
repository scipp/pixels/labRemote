#
# Find libpico
find_package(libpico)

if ( NOT "${LIBPICO_FOUND}" )
  message(STATUS "Disabling libScope due to missing libpico (LIBPICO_FOUND = ${LIBPICO_FOUND})")
  set(libScope_FOUND FALSE PARENT_SCOPE)
  return()
endif()

#
# Prepare the library
add_library(Scope SHARED)
target_sources(Scope
  PRIVATE
  PicoScope.cpp
  PS6000.cpp
  Tektronix654C.cpp
  )
target_include_directories(Scope PUBLIC ${CMAKE_CURRENT_SOURCE_DIR} ${LIBPICO_INCLUDE_DIR} )
target_link_libraries(Scope PRIVATE Com Utils ${LIBPICO_LIBRARIES})

# Tell rest of labRemote that the library exists
set(libScope_FOUND TRUE PARENT_SCOPE)

set_target_properties(Scope PROPERTIES VERSION ${labRemote_VERSION_MAJOR}.${labRemote_VERSION_MINOR})
install(TARGETS Scope)
